const timer = document.querySelector(".timer");
const btnStart = document.querySelector(".btn-start");
const btnPause = document.querySelector(".btn-pause");
const btnReset = document.querySelector(".btn-reset");

let sec = 0;
let min = 0;
let interval;

function renderToDom() {
  timer.innerHTML = `${min < 10 ? "0" : ""}${min}:${sec < 10 ? "0" : ""}${sec}`;
}
const retrievedSec = localStorage.getItem("sec");
const retrievedMin = localStorage.getItem("min");

if (retrievedMin) min = retrievedMin;
if (retrievedSec) sec = retrievedSec;

renderToDom();

const startInterval = function () {
  sec++;
  localStorage.setItem("sec", sec);
  renderToDom();
  if (sec > 59) {
    sec = 0;
    min++;
    localStorage.setItem("min", min);
    timer.innerHTML = `${min}:${sec}`;
  }
};

btnStart.addEventListener("click", () => {
  clearInterval(interval);
  interval = setInterval(startInterval, 1000);
});

btnPause.addEventListener("click", function (e) {
  clearInterval(interval);
});

btnReset.addEventListener("click", function () {
  clearInterval(interval);
  sec = 0;
  min = 0;
  timer.innerHTML = `0${min}:0${sec}`;
  localStorage.clear();
});
